<?php

namespace App\Http\Controllers\Api;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Post;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $average_rating = DB::table('comments')
            ->join('posts','posts.post_id','=','comments.post_id')
            ->select('comments.post_id', DB::raw('AVG(rating) as average_rating'))
            ->groupBy('post_id')
            ->orderBy('post_title','asc')
            ->get();
        return response()->json($average_rating, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Post::where('post_id', $request->input('post_id'))->updateOrCreate($request->all());
        
        $post = Post::firstOrNew(array('post_id' => $request->input('post_id')));
        $post->post_title = $request->input('post_title');
        $post->save();
        $comment = Comment::firstOrNew(array('comment_id' => $request->input('comment_id')));
        $comment->post_id = $request->input('post_id');
        $comment->rating = $request->input('rating');
        $comment->save();
        return response()->json($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return response()->json($comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //Post::where('post_id', $request->input('post_id'))->updateOrCreate($request->all());
        $post = Post::firstOrNew(array('post_id' => $request->input('post_id')));
        $post->post_title = $request->input('post_title');
        $post->save();
        $comment = Comment::firstOrNew(array('comment_id' => $request->input('comment_id')));
        $comment->post_id = $request->input('post_id');
        $comment->rating = $request->input('rating');
        $comment->save();
        return response()->json($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
    }
}
