<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('/averageRating', 'Api\CommentController@index')->name('comments.all');

//Route::get('/comments/{customerId}', 'CustomerController@show')->name('customers.show');

Route::get('/addComment', 'Api\CommentController@store');

