# Laravel-restful-api
#### The Test For Job Applying
## Quick Installation

#### 
``composer install``

#### 
``mv .env.example .env``

#### 
``DB_DATABASE=``
``DB_USERNAME=``
``DB_PASSWORD= ``

#### 
``php artisan key:generate``

#### 
``php artisan migrate``

#### Run At http://localhost:8000 
``php artisan serve``

#### Call API
`` - GET - http://localhost:8000/api/averageRating/ - For Average Rating Of Each Post.``
####
`` - GET - http://localhost:8000/api/addComment?post_id=ID&post_title=TITLE&comment_id=CMTID&rating=RID - For Adding Comment.``

